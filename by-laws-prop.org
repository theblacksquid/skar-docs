
#+TITLE: Organizational Bylaws (proposal)

* Article I: Name and Purpose
** Section 1: Name
   Samahan ng mga Kilusang Anarkistang Rebolusyonaryo (SKAR)
** Section 2: Purpose
   To pursue the cause of Socialism and Anarchism in the Philippines.

   To facilitate and organize the building of a Revolutionary Anarchist
   Federation in the Philippine Archipelago.

* Article II: Membership
** Section 1: Eligibility
*** Subsection 1: Solidarity
    It is the aim of the Organization to bring together the entire working class
    in Solidarity, without exceptions based on race, gender, creed, disability,
    conviction and criminal history. 

    Under no circumstances shall housewives, retirees, working-class students,
    prisoners or unpaid volunteers be excluded from the Organization for the
    simple fact that they do not currently receive wages. 
*** Subsection 2: Exclusions
    The Organization, however, reserves the right to exclude potential members
    based on membership, participation, occupation, etc, that are counter to the
    Organization's goals. These are, but not exclusive to:

    - Membership in Fascist and Ultranationalist Groups
    - Membership and participation in Religious Fundamental Groups and Movements
    - Active Duty in Law Enforcement, Prison Guards, etc, employed by the State
      or by a corporate entity.
    - Membership and/or Active duty in a private army.
    - Membership in an organized crime outfit.
    - Owns a business which employs other workers
    - Officer of a Political Party
** Section 2: Responsibilities
*** Subsection 1: Attendance in the General Assembly
    As there are no central executive committees, the Organization's actions and
    political line is decided and developed within a General Assembly (explained
    in detail in Article III) held every six months. This is in accordance with
    our Charter of Values and Principles (henceforth, The Charter), which
    prevents authoritarian control over the organization by a minority. In
    exchange, each member is given a responsibility to attend and participate in
    the General Assembly.
*** Subsection 2: Member Organizations
    If any organizations want to join the Organization, they must accept the
    Organization's Charter and Bylaws unconditionally. Once they do, they will
    then be recognized as a genuine Focus Group of the Organization, its members
    be recognized as members of the General Assembly and receive support from
    the Organization in their projects and goals so long as their actions are in
    line with our Values and Principles.
*** Subsection 3: Active Participation
    The Organization acts via a number of Focus organizations and through
    groupings with other movements and militants of other political leanings in
    what is called "Grouping of Tendencies" (Tendencies, for short). Independent
    or Collective action within these contexts are needed to not only
    demonstrate one's commitment to the cause but also further the
    Organization's goals moving forward. See Article V for more details.
*** Subsection 4: Levels of Involvement
    The Charter presents three levels of involvement with the Organization's
    work, and any member can move between any of them at any time. And to
    reiterate: A higher level of involvement does not give a member rank or
    authority within the organization. It is simply a reflection of their
    current actions done for the organization.
** Section 3: Resignation and Expulsion
*** Subsection 1: Resignation from the Organization
    Any member can officially cut ties with the organization after sending
    submitting a resignation letter briefly stating their reasons to the
    designated organizational chat server and/or to their focus group
    representative who will then notify the rest of the Federation.
*** Subsection 2: Expulsion from the Organization
    Members found to be in collusion with Law Enforcement, Hate Groups, Fascist
    Groups, etc, or gross neglect and misappropriations of Organization
    resources may be subject to a hearing by way of Special Meeting (Article IV,
    Sec. 2) to be judged by the Assembly. 

    In the case of a guilty verdict, the Chairperson then declares the offending
    member expelled from the organization and calls for a nomination and
    election for whatever post that has been vacated by the expulsion.

* Article III: The General Assembly
** Section 1: Responsibilities
*** Subsection 1: Development of Theory
    For us to remain a cohesive Organization internally, and present a clear set
    of ideas externally to potential supporters, we must formulate and agree on
    principles and analysis of recent events as a collective. 
*** Subsection 2: Development of Strategy
    The engine of revolution is collective action, and for there to be effective
    collective action, we must agree on what things to collectively act upon and
    set the yardsticks by which to measure our successes.
*** Subsection 3: Allocation of Resources
    We must recognize that we as an organization only has a limited pool of
    resources, from funds, manpower to time. We must carefully allocate these
    resources to achieve the most of our goals using what we have.
** Section 2: Frequency
   The General Assembly is to conduct business every six months in order to
   determine if its theories are still applicable, if changes to strategy are
   needed and which projects require additional resources. The Assembly's Agenda
   must be distributed to all of the attendees at least two weeks before the
   given date.
** Section 3: Selection of Coordinators
   A group of members are to be elected to coordinate the General Assembly by
   way of Special Meeting (Article IV, Sec 3) at least a month prior to the
   Assembly date, and must fill the given roles:
*** Subsection 1: Roles
**** Chairperson
     Facilitates the Assembly, brings each point in the Agenda to the attention
     of the Assembly and acknowledges motions raised.
**** Secretary
     Prepares the Agenda, with help from the rest of the Coordinators, based on
     data gathered from the Org's actions, and takes note of the Assembly
     proceedings, which motions were raised by whom, as well as the results of
     said motions.
**** Timekeeper
     Keeps track of the time, of the entire Assembly in general, as well the
     time spent discussing individual motions.
**** Stack-Keeper
     Keeps track of those who raised their hands to speak and in what order.
     This is to make sure that everyone is heard. They also will count votes
     when a motion is raised.
**** Logistics
     Is mainly responsible for the venue, accommodations, etc, of the Assembly,
     with the help of the rest of the coordinators.
*** Subsection 2: Eligibility
    Any active member who has not yet been elected to an Assembly Coordinator
    position previously can be nominated and elected, except in the case of
    everyone already being given a chance at the role. 
* Article IV: Meeting Guidelines
** Section 1: Prescribed Agenda
   The following applies to both the General Assembly proper, special meetings
   as well as meetings done by committees and front organizations under the Org.

*** Subsection 1: Program Flow

    - Call to Order

      This is when the meeting formally starts and personal business is to
      resume after the meeting.

    - Introductions

      The Coordinator team then introduces themselves and their roles, and the
      rest of the meeting is to shake hands with the person seated next to them.

    - Reading and Approval of Previous Minutes

      A summary of the previous meeting's Minutes (if any) is read in order to
      form a sense of continuity between Assemblies as well as to bring to mind
      the current status of the Organization. After reading, The Chairperson
      calls for a vote to approve or amend the Minutes.

    - Follow Up of Previous Business

      The Chairperson calls to attention previously agreed actions to take and
      calls the designated coordinator/contact person for any status updates,
      highlights and points of improvement.

      This is also the space to review previous theories the Assembly or meeting
      agreed upon the during last meeting. Motions to amend and discuss
      political lines in light of current facts are acknowledged by the
      Chairperson and approved by the Assembly.

    - Discussion of Agenda Items

      The Agenda has predefined items for discussion prepared by the current
      Secretary, and is brought to the Assembly's attention.

    - Discussion of New Business

      Motions to review proposals for new long-term programs, activities are
      brought to the Assembly.

      Motions to discuss and analyze news items and reports of relevance to the
      cause are also considered here.

    - Closing Remarks

      The items discussed by the meeting is summarized by either the Chairperson
      or Secretary. The Chairperson then brings the meeting to a close.

*** Subsection 2: Decision Making
    Decisions are arrived at by using Modified Consensus. Assembly members can
    reply with any of the following responses below. 

    If a motion cannot achieve consensus, or the discussion is judged by the
    Timekeeper to be taking too long, motions regarding non-critical matters can
    be "parked", saved for discussion at a different forum. Critical decisions
    that cannot be parked must then be put to a vote requiring a 2/3 majority.

**** Responses:
    - Support

      One agrees and is willing to participate in the given motion.

    - Consent

      One agrees, but is unable to participate in the motion for various
      reasons.

    - Concern

      One has a question about the motion and cannot decide without
      understanding the motion further

    - Disagree

      One is against the motion and has valid reasons for doing so.

**** Consensus

     Consensus is said to be achieved when all the responses are either
     "Supports" or "Consents", with a more than 1/4 of them being the former.

** Section 2: Calling for a Special Meeting
   Special Meetings are called for when important things need to be decided by
   the Assembly and we cannot wait for the next formal meeting. This includes,
   but is not limited to:

   - Expulsion Cases for Violating Members
   - Accepting a proposal to Amend the Bylaws in the next General Assembly
   - Response to overarching threats such as government crackdowns, etc.

* Article V: Focus Groups and Tendencies
** Section 1: Focus Groups
*** Subsection 1: Structure
    Each focus group will be further subdivided into 5-10 member affinity groups
    or teams, with each dedicated to an aspect of the Focus Group's operations,
    either by directly moving towards the Focus Group's stated goals, or by
    supporting those that do.
*** Subsection 2: Operating Teams
    Each Focus group needs to be composed of at least three cells:
    - An Operational Team, that performs the duties of the Focus Group Group.

      Ex. A reading group Focus Group would have meeting facilitators form its
      Operational Teams.

    - A Finance Team, dedicated to managing, auditing and raising funds for the
      operations of the Focus Group Group.

    - And a Public Relations Team, which prepares propaganda materials, promotes
      the aboveground operations of the Focus Group to the public and answers
      inquiries from Media and other external organizations.

    - Any other tasks that require dedicated members may also be set up as teams
      at the discretion of the Focus Group.
*** Subsection 3: Team Rotation
    Every six months, during the General Assembly, responsibilities are rotated
    between the cells within a Focus Group Group, this is to prevent the formation of
    a specialized "coordinator" committee that will soak up the organization's
    power, as well as train everyone in the various tasks necessary to run the
    organization.
*** Subsection 4: Team and Focus Group Meetings
    Checking and coordinating the whole of the Team and the Focus Group Group's
    actions are to be done in the context of a meeting as described in Article
    IV, Section 1, but with the roles rotated instead of elected for Teams, and
    for smaller Focus Group Groups.
** Section 2: Tendency Groups
   This is how the Organization will coordinate and interact with other
   militant, activist or otherwise politically active groups. This is to ensure
   that the goals of the social movements (Gender Equality, Labor Rights, etc)
   are met, as well as add a Libertarian Socialist character to them. This is
   best done by Partisan members (see Charter) who can clearly communicate the
   Organization's position on matters, and thus attract followers from other
   orgs and movements to our cause. While having the support of the movement's,
   org's, Party's, etc, will be helpful, the Partisan's focus must be the
   Rank-And-File members of those organizations, as they will not be as heavily
   invested into their Party's success as the leadership would be, especially in
   the case of Authoritarian and Opportunist organizations.

* Article VI: Amendment of Bylaws
** Section 1: Amendment Proposal
   The bylaws may only be changed by the General Assembly. The motion to change
   the bylaws will only be part of the General Assembly's agenda by way of a
   Special Meeting (Art. IV, Section 2) composed of the General Assembly
   members, or representatives of their Cells, approving of the addition.
** Section 2: Deliberation
   Once part of the General Assembly agenda, the motion is then brought before
   the Assembly and the sponsor of the Motion may then present their case.
   Concerns can then be raised by Assembly members and acknowledged by the
   Chairperson. These concerns are then to be answered by a clarification or
   revision of the original motion. After all concerns are answered, the
   proposal and all revisions are then put to a vote according to Article IV,
   Section 1, Subsection 2 (Decision Making)
